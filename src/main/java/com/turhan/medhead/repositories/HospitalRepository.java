package com.turhan.medhead.repositories;

import com.turhan.medhead.entities.Hospital;
import com.turhan.medhead.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Integer> {

    public List<Hospital> findBySpecialisationsIn(@NonNull Set<String> specialisations);
//    public User findByUsernameEquals(@NonNull String username);

    @Query(value = "SELECT ST_DISTANCE(ST_SETSRID(ST_MAKEPOINT(h.latitude, h.longitude), 4326)::geography," +
                                   "ST_SETSRID(ST_MAKEPOINT(?1, ?2), 4326)::geography) distance" +
            "FROM hospital h", nativeQuery = true)
    public default List<Hospital> findHopitalByLocationAndSpec(Float latitude, Float longitude, int distance, int specialisationId) {
        List<Hospital> hospitals = Arrays.asList();

    }


}
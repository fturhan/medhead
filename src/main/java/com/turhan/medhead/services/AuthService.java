package com.turhan.medhead.services;

import com.turhan.medhead.entities.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class AuthService {

    static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    static public String createJWT(User user) {
// We need a signing key, so we'll create one just for this example. Usually
// the key would be read from your application configuration instead.
        String jws = Jwts.builder()
                .setId(user.getId().toString())
                .claim("username", user.getUsername())
                .claim("email", user.getEmail())
                .signWith(key).compact();
        return jws;
    }

    public User isValid(String jwt) {
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()  // (1)
                    .setSigningKey(key)         // (2)
                    .build()                    // (3)
                    .parseClaimsJws(jwt); // (4)

            User user = new User();
            user.setId(Integer.parseInt(jws.getBody().getId()));
            user.setUsername(jws.getBody().get("username", String.class));
            user.setEmail(jws.getBody().get("email", String.class));
            return user;
            // we can safely trust the JWT
        } catch (JwtException ex) {       // (5)
            return null;
            // we *cannot* use the JWT as intended by its creator
        }
    }
}

package com.turhan.medhead.repositories;

import com.turhan.medhead.entities.Hospital;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class MockHospitalRepository {

    List<Hospital> hospitals;

    @Autowired
    private final MockSpecialisationRepository mockSpecialisationRepository;

    public MockHospitalRepository(MockSpecialisationRepository mockSpecialisationRepository) {
        this.mockSpecialisationRepository = mockSpecialisationRepository;
        hospitals = Arrays.asList(
                new Hospital(1, 140, "la santé", 1F, 1F, Set.of(mockSpecialisationRepository.findById(1).get()), 5F)
        );
    }

    public List<Hospital> findAll() {
        return hospitals;
    }

    public List<Hospital> findHopitalBySpecialisationAndLocation(Float latitude, Float longitude, int distance, int specialisationId) {
        hospitals.stream().filter(hospital -> {
            var hospitalResult = Arrays.asList();
            hospitalResult = hospital.getSpecialisations().stream().filter(specialisation -> {
               return specialisation.getId().equals(specialisationId);
           }).collect(Collectors.toList());
            return hospitalResult;
        });

    }
}

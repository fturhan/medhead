package com.turhan.medhead.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Table(name = "hospital")
@Entity()
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Hospital {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(nullable = false)
    private Integer totalBed;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, precision = 8)
    private Float latitude;

    @Column(nullable = false, precision = 8)
    private Float longitude;

    @ManyToMany
    @JoinColumn(name = "specialisation_id", nullable = false)
    private Set<Specialisation> specialisations;

    @Transient()
    private Float distance;
}
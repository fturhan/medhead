package com.turhan.medhead.controllers;

import com.turhan.medhead.entities.Hospital;
import com.turhan.medhead.entities.Specialisation;
import com.turhan.medhead.repositories.HospitalRepository;
import com.turhan.medhead.services.HospitalService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;
import java.util.List;

@RestController
@RequestMapping("/hospitals")
public class HospitalController {
    private final HospitalService hospitalService;

    public HospitalController(HospitalService hospitalService) {
        this.hospitalService = hospitalService;
    }

    /**
     * Permet de lister les hopitaux pris en charge avec leurs spécialité
     * @return
     */
    @GetMapping
    public List<Hospital> hospitalList() {
        return hospitalService.findAll();
    }


    @GetMapping("/distance")
    public List<Object> distance(@RequestParam Float lat, @RequestParam Float lon, @RequestParam int distance, @RequestParam int specialisationId ) {
        return hospitalService.findHospitalByLocationAndSpec(11.0000F ,1F, 1, specialisationId);
    }

}

package com.turhan.medhead.services;

import com.turhan.medhead.entities.Hospital;
import com.turhan.medhead.entities.Patient;
import com.turhan.medhead.entities.Specialisation;
import com.turhan.medhead.repositories.HospitalRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class HospitalService {
    private final HospitalRepository hospitalRepository;

    public HospitalService(HospitalRepository hospitalRepository) {
        this.hospitalRepository = hospitalRepository;
    }

    public List<Hospital> findByLocation(double lat, double lon) {
        return this.hospitalRepository.findAll();
    }

    public List<Hospital> findAll() {
        return this.hospitalRepository.findAll();
    }

    /**
     * Après avoir choisi le bon hopital, on envoie une demande de reservation de lit dans le bon service.
     * @param patient
     */
    public void reserveBed(Patient patient) {

    }

    /**
     * Permet un premier filtrage des hopitaux disponible en fonction de la position géographique de l'urgence et de la spécialité.
     *
     * @param lat latitude geographique
     * @param lon longitude géographique
     * @param specialisationId    Spécialité médical necessaire pour soigné le patient
     * @return la liste des hopitaux filtrer avec la distance
     */
    public List<Object> findHospitalByLocationAndSpec(Float lat, Float lon, int distance, int specialisationId) {
        return this.hospitalRepository.findHopitalByLocationAndSpec(lat, lon, distance, specialisationId);
    }

    /**
     * Fonction principal du POC
     * Doit permettre de determiner l'hopital en fonction de la localisatioon du patient et de la spécialité dont il a besoin.
     * La liste des hopitaux pouvant acceuillir le patient est filtrer avec la fonctions [distance].
     * Ici on interroge toutes les hopitaux de la liste pour obtenir le nombre de lits disponible en fonction de la spécialité.
     * @param lat latitude geographique
     * @param lon longitude géographique
     * @param distance  distance maximal par rapport à la position géographique
     * @param specialisation    Spécialité médical necessaire pour soigné le patient
     * @return  la liste des hopitaux avec le nombre de lits disponible
     */
    public List<Object> findAvailableBedByLocationAndSpec(double lat, double lon, int distance, Specialisation specialisation) {
        return Arrays.asList();
    }

}

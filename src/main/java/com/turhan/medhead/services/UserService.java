package com.turhan.medhead.services;

import com.turhan.medhead.dtos.UserDTO;
import com.turhan.medhead.entities.User;
import com.turhan.medhead.repositories.UserRepository;
//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Objects;

public class UserService {
    public final UserRepository userRepository;
//    public final ModelMapper modelMapper;
//    @Autowired
    public final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
//        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    public User findUserWithEmailOrUsername(String str) {
        return userRepository.findByEmailOrUsername(str);
    }

    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode((CharSequence) user.getPassword()));
        return userRepository.save(user);
    }

//    public boolean checkPassword(User user, String password) {
//        return Objects.equals(user.getPassword(), passwordEncoder.encode((CharSequence) password));
//    }

//    public User fromUserDTO(UserDTO userDTO) {
//        return modelMapper.map(userDTO, User.class);
//    }
//
//    public UserDTO toUserDTO(User user) {
//        return modelMapper.map(user, UserDTO.class);
//    }
}

package com.turhan.medhead.services;

import com.turhan.medhead.entities.Hospital;
import com.turhan.medhead.entities.Patient;
import com.turhan.medhead.repositories.HospitalRepository;
import com.turhan.medhead.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PatientService {

    @Autowired
    private final PatientRepository patientRepository;

    @Autowired

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Patient createPatient(Patient patient) {
        return patientRepository.save(patient);
    }

    public Patient getPatient(Integer id) {
        return patientRepository.getById(id);
    }

    public void deletePatient(Integer id) {
        patientRepository.deleteById(id);
    }


}

package com.turhan.medhead.repositories;

import com.turhan.medhead.entities.Specialisation;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Repository
public class MockSpecialisationRepository {

    List<Specialisation> specialisations = Collections.emptyList();

    MockSpecialisationRepository() {
        specialisations.add(new Specialisation(1, "Cardiologie"));
        specialisations.add(new Specialisation(2, "Neurologie"));
        specialisations.add(new Specialisation(3, "Medecine général"));
        specialisations.add(new Specialisation(4, "Urologie"));
        specialisations.add(new Specialisation(5, "Chirurgien"));
    }

    Optional<Specialisation> findById(int id) {
        return specialisations.stream().filter(specialisation -> {
            return specialisation.getId().equals(id);
        }).findFirst();
    }

    Optional<Specialisation> findByName(String name) {
        String normalizedName = Normalizer.normalize(name.toLowerCase(), Normalizer.Form.NFC);
        return specialisations.stream().filter(specialisation -> {
            return Normalizer.normalize(specialisation.getName().toLowerCase(), Normalizer.Form.NFC).equals(normalizedName);
        }).findFirst();
    }

}

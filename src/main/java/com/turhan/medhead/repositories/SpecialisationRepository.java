package com.turhan.medhead.repositories;

import com.turhan.medhead.entities.Specialisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecialisationRepository  extends JpaRepository<Specialisation, Integer> {


}

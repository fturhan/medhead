package com.turhan.medhead.controllers;

import com.turhan.medhead.dtos.LoginDTO;
import com.turhan.medhead.entities.User;
import com.turhan.medhead.repositories.UserRepository;
import com.turhan.medhead.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/users")
public class UserController {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    public UserController(UserRepository userRepository, UserService userService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping()
    public List<User> getUser() {
        return userRepository.findAll();
    }

    @PostMapping(value = {"/login"}, consumes = MediaType.APPLICATION_JSON_VALUE)
    public User login(@RequestBody LoginDTO dto) {
        User user = userService.findUserWithEmailOrUsername(dto.getCredential());
        if (user.getPassword().equals(passwordEncoder.encode(dto.getPassword()))) {

        }
        user.set


        return user;
    }

}

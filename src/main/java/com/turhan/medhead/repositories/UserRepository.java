package com.turhan.medhead.repositories;

import com.turhan.medhead.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


    @Query("select u from User u where u.email = ?1 or u.username = ?1")
    public User findByEmailOrUsername(@NonNull String search);

//    public User findByEmailEquals(@NonNull String email);
//    public User findByUsernameEquals(@NonNull String username);
}